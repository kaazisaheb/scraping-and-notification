#!/usr/bin/env python3

import os
import sys

import urllib.request
import urllib.parse
import mimetypes
import datetime
import codecs
import uuid
import json
import time


CHAT_ID = os.environ.get('TELEGRAM_CHAT_ID')

def multipart_encoder(params, files):
    def compose(boundry, params, files):
        for key, val in params.items():
            if val is None: continue
            yield '--' + boundry
            yield 'Content-Disposition: form-data; name="%s"'%key
            yield ''
            yield val

        for key, uri in files.items():
            name = os.path.basename(uri)
            mime = mimetypes.guess_type(uri)[0] or 'application/octet-stream'
            yield '--' + boundry
            yield 'Content-Disposition: form-data; name="{0}"; filename="{1}"'.format(key, name)
            yield 'Content-Type: ' + mime # work without this!
            yield ''
            yield open(uri, 'rb').read()

        yield '--%s--'%boundry

    boundry = uuid.uuid4().hex
    headers = {
        'Content-Type' : 'multipart/form-data; boundary=' + boundry,
    }

    body = bytes()
    for chunk in compose(boundry, params, files):
        if isinstance(chunk, bytes): body += chunk + b'\r\n'
        else: body += bytes(chunk, encoding='utf8') + b'\r\n'
    return headers, body


class Bot:
    def __init__(self, TOKEN):
        self.b2str = codecs.getreader('utf8')
        self.TOKEN = TOKEN


    def callapi(self, method, params=None, headers=None):
        if    isinstance(params, bytes): data = params
        elif  params is None: data = None
        else: data = urllib.parse.urlencode(params).encode()

        request = urllib.request.Request(
            "https://api.telegram.org/bot" + self.TOKEN + method,
            data    = data,
            headers = headers or {},
        )

        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            print(e)
            exit()

        jobj = json.load(self.b2str(response))
        print(json.dumps(jobj, indent=4), file=sys.stderr)
        if not jobj['ok']: return
        return jobj['result']


    def sendMessage(self, message, bcc=None, mode=''):
        if bcc is None: bcc = [ CHAT_ID ]
        for chat_id in bcc:
            self.callapi(
                '/sendMessage',
                params = {
                    'chat_id'    : chat_id,
                    'text'       : message,
                    'parse_mode' : mode,
                }
            )


    def wrap_send_file(self, uri, bcc=None, caption=None, auto_mime=True):
        if bcc is None: bcc = [ CHAT_ID ]
        mime = mimetypes.guess_type(uri)
        if mime[0] and 'image' in mime[0]:
            route = '/sendPhoto'
            ctype = 'photo'
        else:
            route = '/sendDocument'
            ctype = 'document'

        for chat_id in bcc:
            headers, data = multipart_encoder(
                params = {
                    'chat_id'  : chat_id,
                    'caption'  : caption,
                },
                files = {
                    ctype: uri,
                }
            )

            self.callapi(route, data, headers)


    def sendPhoto(uri, bcc=None, caption=None):
        if bcc is None: bcc = [ CHAT_ID ]
        for chat_id in bcc:
            headers, data = multipart_encoder(
                params = {
                    'chat_id'  : chat_id,
                    'caption'  : caption
                },
                files = {
                    'photo': uri,
                }
            )
            self.callapi('/sendPhoto', data, headers)


def get_last_message(n=1):
    # getUpdates.last_update_id =
    update = bot_api(
        '/getUpdates',
        # params = {
        #     'offset' : 1,
        #     # 'timeout' : 1
        # }
    )
    return update[-n]['message']


def cmd_uptime():
    with open('/proc/uptime','r') as f:
        uptime_seconds = float(f.readline().split()[0])
        uptime_string = str(datetime.timedelta(seconds = uptime_seconds))

    return uptime_string


def parser(msg):
    text  = msg['text']
    if text[0] != '/': return

    reply = ""
    if   text == '/uptime': reply = cmd_uptime()
    elif text == '/ping': reply = "pong %s".msg['from']['username']

    return reply, [ msg['chat']['id'] ]


if __name__ == '__main__':
    # info = bot_api('/getMe')
    # info = bot_api('/getUpdates')
    # info = sendPhoto('screenshot18.png', caption='doggi pic')
    bot = Bot(os.environ.get('TELEGRAM_BOT_TOKEN'))
    if len(sys.argv) > 1: bot.sendMessage(' '.join(sys.argv[1:]))
    else: bot.sendMessage("%s testing"%os.uname().nodename)
