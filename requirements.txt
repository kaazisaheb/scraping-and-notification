certifi==2019.11.28
chardet==3.0.4
idna==2.8
lxml==4.4.2
pkg-resources==0.0.0
psycopg2==2.8.4
requests==2.22.0
urllib3==1.25.7
